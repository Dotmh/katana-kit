KATANA KIT
==========

[![CircleCI](https://circleci.com/bb/Dotmh/katana-kit.svg?style=svg&circle-token=6f6ffb2a1859d9be7ac244e655a3171e99e0e2fe)](https://circleci.com/bb/Dotmh/node_ninja)
[![Coverage Status](https://coveralls.io/repos/bitbucket/Dotmh/node_ninja/badge.svg?branch=master)](https://coveralls.io/bitbucket/Dotmh/node_ninja?branch=master)

Katana Kit is a set of standard libraries that can be used to build exciting Node apps. The eventually plan is for the 
libraries in Katana Kit to be split down in to smaller single function modules. This module will then therefore become
a wrapper/helper to include all of Katana Kit in to your application. 

Installing
----------

To install Katana Kit you will need NPM. 

```
npm install --save katana-kit
```
